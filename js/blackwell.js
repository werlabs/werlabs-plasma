

// Hack to make iOS tap == hover
document.getElementsByTagName('body')[0].addEventListener('touchstart', function() {});



// Element.matches polyfill, mainly for IE10 and 11 (https://developer.mozilla.org/en-US/docs/Web/API/Element/matches)
if (!Element.prototype.matches) {
    Element.prototype.matches = Element.prototype.msMatchesSelector || Element.prototype.webkitMatchesSelector;
}



// Language/User menu dropdown
window.addEventListener('click', function (event) {
    if (!event.target.matches('.js-toggle-dropdown')) return;

    event.target.closest('.nav-item--has-dropdown').classList.toggle('dropdown--open');
})



// BankID personal_number validation
// @Todo The button will not re-disable if emptying the input
window.addEventListener('keyup', function (event) {
    if (!event.target.matches('#personal_number')) return;

    const input = event.target;

    const elem = document.getElementById('submit_bankid');

    if (input !== '') {
        elem.removeAttribute('disabled');
        elem.setAttribute('onclick', 'location.href="/pages/login/context.html"')
    } else if (input == '') {
        elem.setAttribute('disabled', 'disabled')
    }
})



// Mobile menu toggle
document.addEventListener('click', function (event) {
    if (!event.target.matches('.js-toggle-nav')) return;

    document.getElementsByTagName('body')[0].classList.toggle('nav--open');
})



// Filter results by year on individual marker group view
// Not very elegant but just for prototype
document.addEventListener('click', function (event) {
    if (event.target.matches('#filter-year_index-0')) {
        const elements = document.querySelectorAll('.js-track-no-1');

        for (var i = 0; i < elements.length; i++) {
            elements[i].classList.toggle('d-none');
        }
    }

    if (event.target.matches('#filter-year_index-1')) {
        const elements = document.querySelectorAll('.js-track-no-2');

        for (var i = 0; i < elements.length; i++) {
            elements[i].classList.toggle('d-none');
        }
    }

    if (event.target.matches('#filter-year_index-2')) {
        const elements = document.querySelectorAll('.js-track-no-3');

        for (var i = 0; i < elements.length; i++) {
            elements[i].classList.toggle('d-none');
        }
    }
})