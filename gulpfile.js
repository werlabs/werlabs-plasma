
const gulp          = require('gulp');
const autoprefixer  = require('gulp-autoprefixer');
const cleanCSS      = require('gulp-clean-css');
const sass          = require('gulp-sass');
const sassGlob      = require('gulp-sass-glob');
const browserSync   = require('browser-sync').create();

// SCSS
function scss() {
    return gulp
        .src('scss/plasma.scss')
        .pipe(sassGlob())
        .pipe(sass().on('error', sass.logError))
        .pipe(autoprefixer())
        .pipe(cleanCSS())
        .pipe(gulp.dest('css'))
        .pipe(browserSync.stream());
}

// Tailwind
function tailwind() {
    const postcss = require('gulp-postcss');

    return gulp
        .src('scss/7-utilities/tailwind.css')
        .pipe(postcss())
        .pipe(cleanCSS())
        .pipe(gulp.dest('css'))
}


// Browsersync
function serve(done) {
    browserSync.init({
        server: {
            baseDir: './'
        }
    });
    done();
}

function reload(done) {
    browserSync.reload();
    done();
}


// Watching...
function watch() {
    gulp.watch(['./tailwind.config.js','./scss/**/*.scss'], scss);
    gulp.watch(['./index.html','./pages/**/*.html'], reload);
    gulp.watch('./js/*.js', reload);
}


function cssDist() {
  return gulp.src('css/**/*.css').pipe(gulp.dest('dist/css/'));
}

function imagesDist() {
  return gulp.src('images/**/*').pipe(gulp.dest('dist/images/'));
}

// Commands
exports.build = gulp.parallel(gulp.series(scss, cssDist), imagesDist);

const dev = gulp.series(scss, serve, watch);
exports.default = dev;


const tw = gulp.series(tailwind);
exports.tw = tw;