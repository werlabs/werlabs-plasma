# Plasma css

Base css for plasma framework

## Installation instructions

Add as npm dependency:

```
npm install git+https://bitbucket.org/werlabs/werlabs-plasma.git#semver:<semver>
```

The distribution css file might be auto detected from the `style` property in `package.json`. If not, import the `dist/css/plasma.css` file directly with your build tool of choice:

```js
import "werlabs-plasma/dist/css/plasma.css";
```

## Tailwind

An opinionated version of Tailwind to match our current helper classes setup to make transition as smooth as possible.

The following plugins have been edited for this:
- alignItems (`align-items-`)
- borderColor (`bc-`)
- borderWidth (`bw-`)
- borderRadius (`br-`)
- display (`d-`)
- fontSize (`fs-`)
- fontWeight (`fw-`)
- lineHeight (`lh-`)
- maxWidth (`mw-`)
- position (`position-`)
- textAlign (`align-`)
- textColor (`color-`)
- textDecoration (`td-`)
- textTransform (`transform-`)

--

**@Todo**

- remove `fonts` directory from repo (fonts should be accessed at `werlabs.se/assets/fonts`).

--

https://github.com/mhulse/mhulse.github.io/wiki/npm-install-from-public-or-private-repos-on-GitHub-or-Bitbucket

https://cloudfour.com/thinks/how-to-distribute-a-pattern-library-as-an-npm-package-from-a-private-git-repo/